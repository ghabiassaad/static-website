module.exports = {
  siteMetadata: {
    title: "static-website",
  },
  plugins: [
    "gatsby-plugin-netlify-cms",
    "gatsby-plugin-sass",
    "gatsby-plugin-gatsby-cloud",
  ],
};
